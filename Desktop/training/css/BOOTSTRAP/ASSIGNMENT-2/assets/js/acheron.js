
$(document).ready(function(){
  var count=0;
  $("form").attr('autocomplete','off');
  $(document).on(" focus, blur",".form-control",function(e){
  if ($(this).val()=="") {
      $(this).addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text("Field is Required..?");
      sub();
    }
    else{
      $(this).addClass("is-valid").removeClass("is-invalid").next().addClass("valid-feedback").removeClass("invalid-feedback").text("Success...!");
         sub();      
     }

  });
  
  function required(cval){
    if(cval.val()==""){
      cval.addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text("Dont leave this field");
      sub();
      return false;

    }
    else{
      return true;
      sub();
    }
    
  }

function f_name(name1)
{
$(document).on('input','#name',function()
{
var name1=$(this);
var nameregex=/^[a-z ,.'-]+$/i;
if(!nameregex.test(name1.val()))
{
console.log("failure");
$(this).addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text("Enter proper first name");
sub();
}
else
{
console.log("success");
$(this).addClass("is-valid").removeClass("is-invalid").next().addClass("valid-feedback").removeClass("invalid-feedback").text("Looks good");
sub();
}
});
}

function e_mail(email1)
{
$(document).on('input','#email',function()
{
var email1=$(this);
var emailregex=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
if(!emailregex.test(email1.val()))
{
console.log("failure");
$(this).addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text("Enter proper email id");
sub();
}
else{
  console.log("success");
$(this).addClass("is-valid").removeClass("is-invalid").next().addClass("valid-feedback").removeClass("invalid-feedback").text("Looks good");
sub();
}
});
}
function c_contact(contact1)
{
$(document).on('input','#contact',function()
{
var contact1=$(this);
var contactregex=/^(\+\d{2,4})?\s?(\d{10})$/;
if(!contactregex.test(contact1.val()))
{
console.log("failure");
$(this).addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text("Enter proper city");
sub();
}
else
{
  console.log("success");
$(this).addClass("is-valid").removeClass("is-invalid").next().addClass("valid-feedback").removeClass("invalid-feedback").text("Looks good");
sub();
}
});
}
function d_degree(degree1)
{
$(document).on('input','#degree',function()
{
var degree1=$(this);
var degreeregex=/^[A-Za-z]+$/;
if(!degreeregex.test(degree1.val()))
{
console.log("failure");
$(this).addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text("Zip code cant be more than 5 digits");
sub();
}
else
{
  console.log("success");
$(this).addClass("is-valid").removeClass("is-invalid").next().addClass("valid-feedback").removeClass("invalid-feedback").text("Looks good");
sub();
}
});
}
function e_exp(exp1)
{
$(document).on('input','#experience',function()
{
var exp1=$(this);
var expregex=/^[0-9]*$/;
if(!expregex.test(exp1.val()))
{
console.log("failure");
$(this).addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text("Enter proper state");
sub();
}
else
{
  console.log("success");
$(this).addClass("is-valid").removeClass("is-invalid").next().addClass("valid-feedback").removeClass("invalid-feedback").text("Looks good");
sub();
}
});
}
function r_role(role1)
{
$(document).on('input','#role',function()
{
var role1=$(this);
var roleregex=/^[A-Za-z]+$/;
if(!roleregex.test(role1.val()))
{
console.log("failure");
$(this).addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text("Enter proper state");
sub();
}
else
{
  console.log("success");
$(this).addClass("is-valid").removeClass("is-invalid").next().addClass("valid-feedback").removeClass("invalid-feedback").text("Looks good");
sub();
}
});
}
function sub() {
            $("form").find(".is-valid").each(function() {
                count++;
                console.log(count);
            });

            (count == 6) ? ($('#submitinput').attr('disabled', false)) : ($('#submitinput').attr('disabled', true));
            count = 0;

        }

        $("#submitinput").click(function() {
            if ($("#name").val() == '') {
                $('#submitinput').attr('disabled', true);
               
            }
            if ($("#email").val() == '') {
                $('#submitinput').attr('disabled', true);
            }
            if ($("#contact").val() == '') {
                $('#submitinput').attr('disabled', true);
            }
            if ($("#degree").val() == '') {               
                $('#submitinput').attr('disabled', true);
             }   
             if ($("#experience").val() == '') {
                $('#submitinput').attr('disabled', true);
   
            }
            if ($("#role").val() == '') {
                $('#submitinput').attr('disabled', true);
            }
            if ($("#customFile").val() == '') {
                $('#submitinput').attr('disabled', true);
            }
            });
$(".form-control").on("input ",function(){

    if(required($(this)))
    {
      $(this).attr("id")=="name"?f_name($(this)):"";
      $(this).attr("id")=="email"?e_mail($(this)):"";
      $(this).attr("id")=="contact"?c_contact($(this)):"";
      $(this).attr("id")=="degree"?d_degree($(this)):"";
      $(this).attr("id")=="experience"?e_exp($(this)):"";
      $(this).attr("id")=="role"?r_role($(this)):"";    
    }
  });
});
